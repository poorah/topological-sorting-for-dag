#include <bits/stdc++.h>

using namespace std ;

class Graph {
    public:
        // properties :
        map<int, bool> visited;
        map<int, list<int>> adj;
        map<int, list<int>> reverseAdj;
        map<int, int> preNum;
        map<int, int> postNum;
        int clock = 1;
        //methods :
        void addEdge(int v, int w);
        void Explore(int v);
        void preVisit(int v);
        void postVisit(int v);
        void showPrePost();
        void DFS();
        void TopologicalOrder();
        void showOrder();
};

void Graph::addEdge(int v, int w) {
    //TODO
    adj[v].push_back(w);
    //adj[w].push_back(v);
}

void Graph::Explore(int v) {
    //TODO
    visited[v] = true ;
    preVisit(v);
    //cout << v << " -> " ;
    list<int>::iterator i;

    for(i = adj[v].begin(); i != adj[v].end(); i++) {
        if(!(visited[*i])) {
            Explore(*i) ;
        }
    }
    
    postVisit(v);
}

void Graph::preVisit(int v) {
    preNum[v] = clock;
    clock++;
}

void Graph::postVisit(int v) {
    postNum[v] = clock;
    clock++;
}

void Graph::DFS() {
    map<int, list<int>>::iterator i;
    
    for(i = adj.begin(); i != adj.end(); i++) {
        if(!(visited[i->first])) {
            Explore(i->first) ;
            //cout << "end" << endl ;
        }
    }
}

// void Graph::showPrePost() {
    
//     map<int, int>::iterator i;
    
//     for(i = preNum.begin(); i != preNum.end(); i++) {
//         cout << "previsit numer node " << i->first << " is : " 
//         << preNum[i->first] << endl ;
        
//         cout << "postvisit numer node " << i->first << " is : " 
//         << postNum[i->first] << endl ;
        
//         cout << "===================\n" ;
//     }
// }

void Graph::showOrder() {
    
    map<int, int>::iterator i;
    map<int, int>::iterator j;
    
    int node = 0;
    
    for(i = postNum.begin(); i != postNum.end(); i++) {
        int max = INT_MIN;
        for(j = postNum.begin(); j != postNum.end(); j++) {
            if(postNum[j->first] > max) {
                max = postNum[j->first];
                node = j->first;
            }
        }
        cout << node << " -> ";
        postNum[node] = INT_MIN;
    }
}

void Graph::TopologicalOrder() {
    
    DFS();
    showOrder();
}


int main() {

    Graph g ;
    
    
    g.addEdge(1, 2);
    g.addEdge(2, 3);
    g.addEdge(1, 4);

    g.TopologicalOrder() ;

    
    
    return 0;
}
